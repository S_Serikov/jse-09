package ru.serikov.tm;

import ru.serikov.tm.controller.ProjectController;
import ru.serikov.tm.controller.SystemController;
import ru.serikov.tm.controller.TaskController;
import ru.serikov.tm.entity.Task;
import ru.serikov.tm.repository.ProjectRepository;
import ru.serikov.tm.repository.TaskRepository;
import ru.serikov.tm.service.ProjectService;
import ru.serikov.tm.service.TaskService;

import java.util.Scanner;

import static ru.serikov.tm.constant.TerminalConst.*;

/**
 * Тестовое приложение
 */
public class App {

    private ProjectRepository projectRepository = new ProjectRepository();

    private ProjectService projectService = new ProjectService(projectRepository);

    private final ProjectController projectController = new ProjectController(projectService);

    private TaskRepository taskRepository = new TaskRepository();

    private TaskService taskService = new TaskService(taskRepository);

    private TaskController taskController = new TaskController(taskService);

    private final SystemController systemController = new SystemController();

     {
        projectRepository.create("DEMO PROJECT 1");
        projectRepository.create("DEMO PROJECT 2");
        projectRepository.create("DEMO PROJECT 3");
        taskRepository.create("TEST TASK 1");
        taskRepository.create("TEST TASK 2");
        taskRepository.create("TEST TASK 3");
    }

    public static void main(final String[] args) {
        final App app = new App();
        app.run(args);
        app.systemController.displayWelcome();
        app.process();
    }

    public void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!CMD_EXIT.equals(command)) {
            command = scanner.nextLine();
            run(command);
            System.out.println();
        }
    }

    public void run (final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int  result = run(param);
        System.exit(result);
    }

    public int run (final String param) {
        if (param == null || param.isEmpty()) return -1;
        switch (param) {
            case CMD_VERSION: return systemController.displayVersion();
            case CMD_ABOUT: return systemController.displayAbout();
            case CMD_HELP: return systemController.displayHelp ();
            case CMD_EXIT: return systemController.Exit ();

            case CMD_PROJECT_CREATE: return projectController.createProject();
            case CMD_PROJECT_CLEAR: return projectController.clearProject();
            case CMD_PROJECT_LIST: return projectController.listProject();
            case CMD_PROJECT_VIEW_BY_INDEX: return projectController.viewProjectByIndex();
            case CMD_PROJECT_VIEW_BY_ID: return projectController.viewProjectById();
            case CMD_PROJECT_REMOVE_BY_ID: return projectController.removeProjectById();
            case CMD_PROJECT_REMOVE_BY_NAME: return projectController.removeProjectByName();
            case CMD_PROJECT_REMOVE_BY_INDEX: return projectController.removeProjectByIndex();
            case CMD_PROJECT_UPDATE_BY_INDEX: return projectController.updateProjectByIndex();
            case CMD_PROJECT_UPDATE_BY_ID: return projectController.updateProjectById();

            case CMD_TASK_CREATE: return taskController.createTask();
            case CMD_TASK_CLEAR: return taskController.clearTask();
            case CMD_TASK_LIST: return taskController.listTask();
            case CMD_TASK_VIEW_BY_INDEX: return taskController.viewTaskByIndex();
            case CMD_TASK_VIEW_BY_ID: return taskController.viewTaskById();
            case CMD_TASK_REMOVE_BY_ID: return taskController.removeTaskById();
            case CMD_TASK_REMOVE_BY_NAME: return taskController.removeTaskByName();
            case CMD_TASK_REMOVE_BY_INDEX: return taskController.removeTaskByIndex();
            case CMD_TASK_UPDATE_BY_INDEX: return taskController.updateTaskByIndex();
            case CMD_TASK_UPDATE_BY_ID: return taskController.updateTaskById();

            default: return systemController.displayError();
        }
    }

}
